from datetime import datetime
from footyframe import utils

class Player:

    def __init__(self, id=None, name=None):
        self.id        = None
        self.name      = None
        self.position  = None
        self.country   = None
        self.birthday  = None
        self.shirt     = None
        self.height    = None
        self.weight    = None
        self.twitter   = None
        self.instagram = None

    def age(self, date=None):
        print(f"Checking {date} - {self.birthday}")
        if self.birthday==None: return None
        # If no date is given, use today's date.
        if date is None:  refdate = datetime.now().date()
        else:             refdate = utils.parse_date(date)
        # Calculate age by subtracting the birth year from the current year.
        age = refdate.year - self.birthday.year
        # If player's birthday has not occurred this year, subtract 1.
        if (refdate.month, refdate.day) < (self.birthday.month, self.birthday.day):
            age -= 1
        return age

    def set_birthday(self, date):
        self.birthday = utils.parse_date(date)

    def __str__(self):
        return f"{self.id}:{self.name}"

    def __repr__(self):
        return f"<Player(id={self.id}, name={self.name})>"
