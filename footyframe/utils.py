import datetime
import logging

logger = logging.getLogger()

def parse_date(date):
    if isinstance(date, datetime.datetime):
        return date.date()
    elif isinstance(date, datetime.date):
        return date
    for fmt in ("%Y-%m-%d", "%d/%m/%Y", "%Y%m%d"):
        try:
            return datetime.datetime.strptime(date, fmt).date()
        except ValueError:
            continue
    logger.debug(f"Unable to parse date value '{date}'")
    return False

