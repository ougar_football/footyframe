import pytest
import datetime
from datetime import datetime as dt
from footyframe.player import Player

def test_age_current():
    # Create a player with a known birthday
    player = Player(id=1, name="John Doe")
    player.set_birthday("1990-05-15")
    # Calculate expected age for today
    expected_age_today = dt.now().year - 1990 - ((dt.now().month, dt.now().day) < (5, 15))
    assert player.age() == expected_age_today

@pytest.mark.parametrize("test_date, expected_age", [
    ("2023-05-14", 32),                          # before birthday in 2023
    ("2023-05-16", 33),                          # after birthday in 2023
    (datetime.date(2010,10,10), 20),             # Call with datetime.date object
    (datetime.datetime(2011,4,10,23,50,49), 20), # Call with datetime.datetime object
    ("1/10/2020", 30),                           # Different date format
    ("20210410", 30),                            # Last date format
])
def test_age(test_date, expected_age):
    player = Player(id=1, name="John Doe")
    player.set_birthday("1990-05-15")
    assert player.age(test_date) == expected_age
