# test_utils.py

import pytest
import footyframe.utils as utils
from datetime import datetime, date

def test_parse_date_with_datetime():
    dt = datetime(2023, 11, 6, 12, 0)
    expected_date = date(2023, 11, 6)
    assert utils.parse_date(dt) == expected_date

def test_with_date():
    d = date(2023, 11, 6)
    assert utils.parse_date(d) == d

@pytest.mark.parametrize("date_str,expected_date", [
    ('2023-11-06', date(2023, 11, 6)),
    ('06/11/2023', date(2023, 11, 6)),
    ('20231106', date(2023, 11, 6)),
])
def test_with_string_correct_format(date_str, expected_date):
    assert utils.parse_date(date_str) == expected_date

def test_with_string_incorrect_format():
    assert utils.parse_date('2023-02-34') is False  # Invalid date
    assert utils.parse_date('11-06-2023') is False  # MM-DD-YYYY is not supported
    assert utils.parse_date('not a date') is False 

